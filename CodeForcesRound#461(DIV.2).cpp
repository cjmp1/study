// A.CPP / if / O(1)
// 원본이 한 번 이상 복사되야 복사본이 늘어날 수 있다.
#include <stdio.h>
int main(){
    int a,b; scanf("%d %d",&a,&b);
    if(b > 0 && (b > 1 || a == 0) && ((a+b)%2 && a+1 >= b)) printf("Yes");
    else printf("No");
    return 0;
}
//B.cpp
// o(n^2)
#include <stdio.h>
int A[22],B[22];
int main(){
    int n,x,a,b,l,ans = 0,s; scanf("%d",&n);
    for(int i = 1; i <= n; i++){
        for(int j = i; j <= n; j++){
            a = b = 0;  s = 1; l = 0;
            x = i; while(x){
                A[++a] = x&1;
                x>>=1;
            }
            x = j; while(x){
                B[++b] = x&1;
                x>>=1;
            }
            for(int k = 1; k <= (a < b ? b : a); k++){
                if(A[k] != B[k]) l += s;
                s<<=1;
                A[k] = B[k] = 0;
            }
            if(l >= j && l <= n && (i + j) > l){
               // printf("%d %d %d\n",i,j,l);
                ans++;
            }
        }
    }
    printf("%d\n",ans);
    return 0;
}
// C.cpp
// o(k)
#include <stdio.h>
int main(){
    __int64 n,k; int i;
    scanf("%I64d %I64d",&n,&k);
    for(i = 1; i <= k; i++){
        if(n%i != i-1){ printf("No\n"); break; }
    }
    if(i == k+1) printf("Yes\n");
    return 0;
}
// D.cpp
// o(n lg n)
#include "bits/stdc++.h"
using namespace std;
vector<string> s;
long long f(string &a){
	long long c = 0, ans = 0;
	string::iterator i;
	for(i = a.begin(); i != a.end(); ++i){
	    c = *i == 's' ? c+1 : c;
	    ans = *i == 's' ? ans : ans + c;
	}
	return ans;
}
bool cmp(string &x,string &y){
	string s1 = x+y, s2 = y+x;
	return f(s1) > f(s2);
}
int main(){
	ios::sync_with_stdio(false);
	int n; string a; cin >> n;
	while(n--) cin >> a , s.push_back(a);
	sort(s.begin(), s.end(), cmp);
	vector<string>::iterator i;
	string ans = "";
	for(i = s.begin(); i != s.end(); ++i) ans += *i;
	cout << f(ans) << endl;
	return 0;
}
// E.cpp
// easyDP o(n*MAXM(10^4)*c) DP[N][C]
// hardDP o(nc) DP[SUM(C)]
#include <stdio.h>
#include <memory.h>
typedef long long ll;
ll dp[10011];
int birds[1011], costs[1011], cnt = 0;
void chk(int p,ll cost){
    cost *= p;
    for(int j = cnt; j >= 0; j--){
        if(dp[j] >= cost){
            if(dp[j+p] < (dp[j]-cost)) dp[j+p] = dp[j] - cost;
        }
    }
}
int main(){
    int n,w,b,x; scanf("%d %d %d %d",&n,&w,&b,&x);
    int bsize = 0;
    for(int i = 1; i <= n; i++){
        scanf("%d",&birds[i]);
        bsize += birds[i];
    }
    for(int i = 1; i <= n; i++) scanf("%d",&costs[i]);
    for(int i = 0; i<= bsize; i++) dp[i] = -1 * (ll)1000000000000;
    //printf("\n");
    dp[0] = w;
    for(int i = 1; i <= n; i++){
        int p = 1, left = birds[i];
        while(p <= left){
            chk(p, costs[i]);
            cnt += p; left -= p;
            p<<=1;
        }
        if(left){
            chk(left, costs[i]);
            cnt += left;
        }
        for(int j = cnt; j >= 0; j--){
            if(dp[j] >= 0){
                dp[j] = (w + (ll)j * b) > (dp[j] + x) ? (dp[j] + x) : (w +(ll)j * b);
            }
        }
          //for(int j =0; j <= cnt; j++) printf("%lld ",dp[j]);
         // printf("\n");
    }
    int ans = 0;
    for(int j = cnt; j >= 0; j--){
        if(dp[j] >= 0){ ans = j; break; }
    }
    printf("%d\n",ans);
    return 0;
}
// f.cpp