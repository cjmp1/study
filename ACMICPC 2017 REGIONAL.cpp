// C.CPP
#include <iostream>
#include <vector>
#include <deque>
#include <algorithm>
using namespace std;
const int maxn = 100111;
typedef pair<int,int> pii;
vector<int> con[maxn];
deque<pii> vertex;
int dp[maxn],ans;
bool compare(pii x,pii y){
    if(x.second > y.second) return 0;
    return 1;
}
int main(){
    ios::sync_with_stdio(false);
    int n,m; cin >> n >> m;
    for(int i = 1; i <= m; i++){
        int a,b; cin >> a >> b;
        a++; b++;
        con[a].push_back(b); con[b].push_back(a);
    }
    for(int i = 1; i <= n; i++){ dp[i] = 1; vertex.push_back(make_pair(i,con[i].size())); }
    sort(vertex.begin(),vertex.end(),compare);
    while(!vertex.empty()){
        int v = vertex.front().first; vertex.pop_front();
        for(int i = 0; i < con[v].size(); i++){
            if(con[con[v][i]].size() > con[v].size()){
                dp[con[v][i]] = dp[con[v][i]] < (dp[v]+1) ? (dp[v]+1) : dp[con[v][i]];
            }
        }
    }
    for(int i = 1; i <= n; i++) ans = ans < dp[i] ? dp[i] : ans;
    cout << ans << endl;
    return 0;
}
//D.CPP
#include <bits/stdc++.h>
using namespace std;
bool chk[800];
int f(int x){
        int y = 0;
        while(true){
                y = y +((x%10) * (x%10));
                x = x / 10;
                if(x == 0) break;
        }
        return y;
}
int main(){
        int n; scanf("%d",&n);
        n = f(n);
        chk[n] = true;
        bool flag = true;
        while(true){
                n = f(n);
                if(chk[n]){
                        if(n == 1) flag = false;
                        break;
                }
                chk[n] = true;
        }
        if(flag) printf("UNHAPPY\n");
        else printf("HAPPY\n");
        return 0;
}
//F.CPP
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
int pattern[4][4],ptype[4][4],n,m;
typedef pair<int,int> pii;
vector<pii> pcycle;
void dv(int x,int size,int npt){
    if(size == 1) return;
    if((x + size / 4 - 1) >= m){ pcycle.push_back(make_pair(npt,0)); dv(x,size/4,pattern[npt][0]); }
    else if((x + size / 2 - 1) >= m){ pcycle.push_back(make_pair(npt,1)); dv(x+size/4,size/4,pattern[npt][1]); }
    else if((x + (size / 4) * 3 - 1) >= m){ pcycle.push_back(make_pair(npt,2)); dv(x+size/2,size/4,pattern[npt][2]); }
    else{ pcycle.push_back(make_pair(npt,3)); dv(x+(size/4)*3,size/4,pattern[npt][3]); }
    // pattern
    //1 - ld1 - lu2 - ru3 - rd4 p1
    //2 - ld(ld1 - rd2 - ru3 - lu4) - lu(ld1 - lu2 - ru3 - rd4) - ru(ld1 - lu2 - ru3 - rd4) - rd(ru1 - lu2 - ld3 - rd4)
                   //p2                           p1                        p1                          p3
    //3 - ld(ld(ld - lu - ru - rd) - rd(ld - rd - ru - lu) - ru(ld - rd - ru - lu) - lu(ru - rd - ld - lu))
                                                                                             //p4
    //p1(p2(p1(p2-p1-p1-p3)-p2-p2-p4)-p1(p2-p1-p1-p3)-p1(p2-p1-p1-p3)-p3(p4-p3-p3-p1))
    //0 - p1
    //1 - p1 -> p2-p1-p1-p3
    //2 - p2 -> p1-p2-p2-p4 , p1->p2-p1-p1-p3 , p3-> p4-p3-p3-p1
    //3 - p1 -> p2-p1-p1-p3 , p2->p1-p2-p2-p4 , p3-> p4-p3-p3-p1 , p4-> p3->p4->p4->p2
    
    //p1(0,3) -> p3(2,2) -> p3(2,2)
}
void init(){
    pattern[0][0] = 1; pattern[0][1]  = 0; pattern[0][2] = 0; pattern[0][3] = 2;
    pattern[1][0] = 0; pattern[1][1]  = 1; pattern[1][2] = 1; pattern[1][3] = 3;
    pattern[2][0] = 3; pattern[2][1]  = 2; pattern[2][2] = 2; pattern[2][3] = 0;
    pattern[3][0] = 2; pattern[3][1]  = 3; pattern[3][2] = 3; pattern[3][3] = 1;
    
    ptype[0][0] = 0; ptype[0][1] = 1; ptype[0][2] = 2; ptype[0][3] = 3;
    ptype[1][0] = 0; ptype[1][1] = 3; ptype[1][2] = 2; ptype[1][3] = 1;
    ptype[2][0] = 2; ptype[2][1] = 1; ptype[2][2] = 0; ptype[2][3] = 3;
    ptype[3][0] = 2; ptype[3][1] = 3; ptype[3][2] = 0; ptype[3][3] = 1;
}
int main(){
    ios::sync_with_stdio(false);
    cin >> n >> m;
    init();
    //m = 60; n = 8;
    //while(m <= n * n){
        pcycle.clear();
        dv(1,n*n,0);
        int ax,ay; ax = ay = 1;
        int ns = n >> 1;
        vector<pii>::iterator it;
        for(it = pcycle.begin(); it != pcycle.end(); ++it){
            //cout << it->first << " " << it->second << endl;
            if(ptype[it->first][it->second] == 0){}
            else if(ptype[it->first][it->second] == 1) ay = ay + ns;
            else if(ptype[it->first][it->second] == 2){ ax = ax + ns; ay = ay + ns;}
            else ax = ax + ns;
            ns >>= 1;
        }
        //cout << "--" << endl;
        cout << ax << " " << ay << endl;
        //m++;
    //}
    return 0;
}
//H.cpp
#define _USE_MATH_DEFINES
#include <bits/stdc++.h>
using namespace std;
 
#define MAXN 100005
#define sz(v) ((int)(v).size())
#define all(v) (v).begin(),(v).end()
typedef complex<double> base;
  
void fft(vector <base> &a, bool invert)
{
    int n = sz(a);
    for (int i=1,j=0;i<n;i++){
        int bit = n >> 1;
        for (;j>=bit;bit>>=1) j -= bit;
        j += bit;
        if (i < j) swap(a[i],a[j]);
    }
    for (int len=2;len<=n;len<<=1){
        double ang = 2*M_PI/len*(invert?-1:1);
        base wlen(cos(ang),sin(ang));
        for (int i=0;i<n;i+=len){
            base w(1);
            for (int j=0;j<len/2;j++){
                base u = a[i+j], v = a[i+j+len/2]*w;
                a[i+j] = u+v;
                a[i+j+len/2] = u-v;
                w *= wlen;
            }
        }
    }
    if (invert){
        for (int i=0;i<n;i++) a[i] /= n;
    }
}
  
void multiply(const vector<int> &a,const vector<int> &b,vector<int> &res)
{
    vector <base> fa(all(a)), fb(all(b));
    int n = 1;
    while (n < max(sz(a),sz(b))) n <<= 1;
    fa.resize(n); fb.resize(n);
    fft(fa,false); fft(fb,false);
    for (int i=0;i<n;i++) fa[i] *= fb[i];
    fft(fa,true);
    res.resize(n);
    for (int i=0;i<n;i++) res[i] = int(fa[i].real()+(fa[i].real()>0?0.5:-0.5));
}
 
int N, M;
int code[128];
char A[MAXN], B[MAXN];
 
int main()
{
    code['R'] = 0, code['P'] = 1, code['S'] = 2;
    scanf("%d%d%s%s", &N, &M, A, B);
    vector <int> total(N+M, 0);
    for (int i=0;i<N;i++) A[i] = (code[A[i]]+1)%3;
    for (int i=0;i<M;i++) B[i] = code[B[i]];
    for (int i=0;i<3;i++){
        vector <int> a(N+M, 0), b(M, 0), res;
        for (int j=0;j<N;j++) a[j] = (A[j] == i);
        for (int j=0;j<M;j++) b[j] = (B[M-j-1] == i);
        //for(int j =0; j < N; j++) cout << a[j] << " ";
        //cout << endl;
        //for(int j =0;j< M;j++) cout << b[j] << " ";
        //cout << endl;
        multiply(a, b, res);
        for (int j=0;j<N+M;j++) total[j] += res[j];
        //for(int j =0; j <N+M; j++) cout << res[j] << " ";
        //cout << endl;
        //cout << "----------------" << endl;
    }
    int ans = 0;
    for (int i=M-1;i<N+M;i++) ans = max(ans, total[i]);
    printf("%d\n", ans);
    return 0;
}
//K.cpp
#include <bits/stdc++.h>
using namespace std;
 
#define MAXN 10004
 
int N;
int A[MAXN];
 
int main()
{
    scanf("%d", &N);
    for (int i=1;i<=N;i++) scanf("%*d%d", A+i);
    for (int i=1;i<=N;i++) printf("%d ", A[i] == A[i+1] ? N-i+1 : 1); puts("");
}
//B.cpp
#include <iostream>
using namespace std;
const int MAX_way = 43046721; // 3^16
int tab[16], p, Y, X, ans;
bool chk[MAX_way],chkc[MAX_way];
char cache[MAX_way];
int getn(int x,int y){ return (y-1)*4 + (x-1); } // 0 ~ 15
int gy[]={0,1,1,1},gx[]={1,1,0,-1};
inline int can(int cstate){
    char &ret = cache[cstate];
    if(chkc[cstate]) return ret;
    chkc[cstate] = true;
    int map[5][5]={0,};
    for(int i = 1; i <= 4; i++){
        for(int j = 1; j <= 4; j++){
            int t = getn(i,j);
            map[i][j] = cstate / tab[t] % 3;
        }
    }
    for(int i = 1; i <= 4; i++){
        for(int j = 1; j <= 4; j++){
            if(map[i][j]){
                for(int k = 0; k < 4; k++){
                    int y = i, x = j, cnt = 0;
                    for(;;){
                        if(y > 4 || x < 1 || x > 4 || map[i][j] != map[y][x]) break;
                        cnt++;
                        y += gy[k]; x += gx[k];
                    }
                    if(cnt > 2) return ret = map[i][j];
                }
            }
        }
    }
    return ret = 0;

}
void dfs(int state, int turn){
    //cout << state << " " << turn << " " << chk[state] << endl;
    if(chk[state]) return;
    chk[state] = true;
    int map[5][5]={0,};
    for(int i = 1; i <= 4; i++){ 
        for(int j = 1; j <= 4; j++){
            int t = getn(i,j);
            map[i][j] = state / tab[t] % 3;
            //cout << map[i][j] << " ";
        }
    //    cout << endl;
    }
    for(int j = 1; j <= 4; j++){
        for(int i = 1; i <= 4; i++){
            if(!map[i][j]){
                int t = getn(i,j);
                int nstate = state + tab[t] * turn;
                int res = can(nstate);
                if(res){
                    if(i == Y && j == X && res == 2){
                        ans++;
                        //for(int ii = 1; ii <= 4; ii++){for(int jj = 1; jj <= 4; jj++){ cout << map[ii][jj] << " "; } cout << endl; }
                        //cout << "---------------------" << endl;
                    }
                    break;
                }
                dfs(nstate, 3-turn);
                break;
            }
        }
    }
}
int main(){
    ios::sync_with_stdio(false);
    tab[0] = 1;
    for(int i = 1; i < 16; i++) tab[i] = tab[i-1] * 3; // 3^0 ~ 3^15
    cin >> p >> Y >> X;
    dfs(tab[getn(1,p)], 2);
    cout << ans << endl;
    return 0;
}
//I.cpp
#include <iostream>
using namespace std;
const int maxn = 1000011;
int n,s[maxn],f[maxn],p[maxn];
void failure(){
    int i = 0, j = -1;
    f[i] = -1;
    while(i < n){
        if(j == -1 || p[i] == p[j]){
            i++; j++; // i = 5 j = 3
            f[i] = j;
        }
        else j = f[j];
    }
}
int main(){
    ios::sync_with_stdio(false);
    cin >> n; for(int i = 0; i < n; i++){
        cin >> s[i];
    }for(int i = 0; i < n; i++){
        p[i] = s[n-i-1];
       // cout << p[i] << " ";
    }
   // cout << endl;
    failure();
    int ans = n - f[0], pns = 0 - f[0];
    //cout << f[0] << " ";
    for(int i = 1; i <= n; i++){
        //cout << f[i] << " ";
        if(ans > n-f[i]){
            ans = n-f[i];
            pns = i-f[i];
        }
    }
   // cout << endl;
    cout << ans-pns << " " << pns << endl;
    return 0;
}
//G.CPP
#include <bits/stdc++.h>
using namespace std;
 
#define MAXN 25003
typedef long long lld;
 
int N, M;
int X1[MAXN], Y1[MAXN], X2[MAXN], Y2[MAXN];
int C; lld A;
 
int main()
{
    scanf("%d%d", &N, &M);
    scanf("%d", Y1); for (int i=1;i<=N;i++) scanf("%d%d", X1+i, Y1+i);
    scanf("%d", Y2); for (int i=1;i<=M;i++) scanf("%d%d", X2+i, Y2+i);
    lld tmp = 0; bool has_closed = Y1[0] > Y2[0];
    for (int i=1,j=1,prv=-1;i<=N||j<=M;){
        if (j > M || i <= N && X1[i] < X2[j]){
            if (prv >= 0 && Y1[i-1] < Y2[j-1]) tmp += (lld)(Y2[j-1]-Y1[i-1])*(X1[i]-prv);
            if (Y1[i] > Y2[j-1]){
                if (has_closed && tmp) A += tmp, C++;
                tmp = 0; has_closed = 1;
            }
            prv = X1[i++];
        }else{
            if (prv >= 0 && Y1[i-1] < Y2[j-1]) tmp += (lld)(Y2[j-1]-Y1[i-1])*(X2[j]-prv);
            if (Y1[i-1] > Y2[j]){
                if (has_closed && tmp) A += tmp, C++;
                tmp = 0; has_closed = 1;
            }
            prv = X2[j++];
        }
    }
    printf("%d %lld\n", C, A);
    return 0;
}