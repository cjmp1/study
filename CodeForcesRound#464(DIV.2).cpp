//A.CPP
#include <iostream>
using namespace std;
const int maxn = 5011;
int f[maxn];
int main(){
    ios::sync_with_stdio(false);
    int n; cin >> n;
    for(int i = 1; i <= n; i++){ cin >> f[i]; }
    bool flag = false;
    for(int i = 1; i <= n; i++){
        if(f[f[f[i]]] == i){
            flag = true; cout << "YES" << endl;
            break;
        }
    }
    if(!flag) cout << "NO" << endl;
    return 0;
}
//B.CPP
#include <iostream>
#include <algorithm>
using namespace std;
const int maxk = 100011;
long long a[maxk];
pair<int, long long> ans;
int main(){
    ios::sync_with_stdio(false);
    long long n,k,mx; cin >> n >> k;
    for(int i = 1; i <= k; i++){
        cin >> a[i];
        long long capacity = (n/a[i]) * a[i];
        if(mx < capacity || i == 1){
            mx = capacity;
            ans = make_pair(i,n / a[i]);
        }
    }
    cout << ans.first << " " << ans.second << endl;
    return 0;
}
//C.CPP
#include <iostream>
using namespace std;
int main(){
    ios::sync_with_stdio(false);
    int n, s, f, ans = 0, p = 0, da = 0;
    cin>>n;
    int a[n];
    for (int i = 1; i <= n; i++){ cin >> a[i%n]; }
    cin >> s >> f;
    for (int i = 1; i < n; i++){
        int l = (s - i + n) % n;
        int r = (f - i + n) % n;
        //cout << l << " " << r << " " << a[l] << " " << a[r] << endl;
        ans = ans + a[l] - a[r];
        if(ans > da){ da = ans, p = i; }
    }
    cout << p+1 << endl;
    return 0;
}