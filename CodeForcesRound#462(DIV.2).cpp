//A.cpp
// o(nm) , for
#include <stdio.h>
__int64 a[66];
__int64 b[66];
int main(){
    int n,m; scanf("%d %d",&n,&m);
    for(int i = 1; i <= n; i++){
        scanf("%I64d",&a[i]);
    }for(int i = 1; i <= m; i++){
        scanf("%I64d",&b[i]);
    }
    __int64 x,y;
    x = a[1] * b[1];
    itn k = 1;
    for(int i = 1; i <= n; i++){
        for(int j = 1; j <= m; j++){
            if(x < a[i] * b[j]){
                x = a[i] * b[j];
                k = i;
            }
        }
    }
    if(k == 1) y = a[2] * b[1];
    else y = a[1] * b[1];
    for(int i = 1; i <= n; i++){
        if(i != k){
        for(int j = 1; j <= m; j++){
            if(y < a[i] * b[j]){
                y = a[i] * b[j];
            }
        }
        }
    }
    printf("%I64d\n",y);
    return 0;
}
//B.cpp
// o(c) c is const , math,if
#include <stdio.h>
int main(){
    int k; scanf("%d",&k);
    int a = k/2;
    int b = k&1;
    if(k > 36) printf("-1");
    else{
        for(int i = 1; i <= a; i++){printf("8");}
        if(b) printf("4");
    }
    return 0;
}
//C.cpp
// o(n^2) , for
#include <stdio.h>
#define MAXN 2222
int inp[MAXN],left[MAXN],right[MAXN];
int main() {
    int n,ans = 0; scanf("%d",&n);
	for (int i = 1; i <= n; i++){ scanf("%d", &inp[i]); }
	for (int i = 1; i <= n; i++) { left[i] = left[i-1] + (inp[i] == 1); }
	for (int i = n; i >= 1; i--) { right[i] = right[i+1] + (inp[i] == 2); }
	for (int i = 1; i <= n; i++) {
		int cnt = 0, one = 0, m = 0;
	    // (i ~ j) reverse
		for (int j = i; j <= n; j++) {
			if (inp[j] == 1) cnt--, one++;
			else cnt++;
			m = m < cnt ? cnt : m;
			int p = left[i-1] + one + m + right[j+1];
			ans = ans < p ? p : ans;
		}
	}
	printf("%d\n", ans);
	return 0;
}
//D.cpp
// o(log_k(n))
#include<bits/stdc++.h>
#define int long long
using namespace std;
int n,k,ans[2010];
main(){
	cin>>n>>k; k=-k;
	int cnt=0; 
	while (n){
		ans[cnt]=n%k;
		//cout << "ans[cnt] = n%k " << ans[cnt] << endl;
		n/=k;
		//cout << "n/=k " << n << endl;
		if (ans[cnt]<0){
			ans[cnt]-=k;
			//cout << "ans[cnt]-=k " << ans[cnt] << endl;
			++n; 
			//cout << "++n " << n << endl;
		}
		++cnt;
		//cout << "++cnt " << cnt << endl;
	}
	cout<<cnt<<endl;
	for (int i=0; i<cnt; ++i) cout<<ans[i]<<" ";
}
//E.cpp
// not solved