// A.cpp
// o(n)
#include <stdio.h>
int main(){
    int n,m,a,b; scanf("%d %d",&n,&m);
    double ans = -1;
    for(int i = 1; i <= n; i++){
        scanf("%d %d",&a,&b);
        double c = (double)a / (double)b * (double)m;
        if(ans == -1) ans = c;
        else ans = ans > c ? c : ans;
    }
    printf("%.8f\n",ans);
    return 0;
}
// B.cpp
// o(9 ^ 10)
#include <stdio.h>
int cnt = 0, n;
bool flag = false;
void dfs(int cur, int hap, int len){
    if(hap == 10 && len == 10){
        cnt++; if(cnt == n){
            printf("%d\n",cur);
            flag = true;
        }
        return;
    }
    else if(len == 10) return;
    for(int i = 0; i < 10; i++){
        dfs(cur * 10 + i, hap + i, len + 1);
        if(flag) return;
    }
}
int main(){
    scanf("%d",&n);
    dfs(0,0,0);
    return 0;
}
// C.cpp
// o(n ^ 2)
#include <stdio.h>
const int MAXN = 2011;
char map[MAXN][MAXN];
int row[MAXN][MAXN],col[MAXN][MAXN];
int main(){
    int n,m,k,ans = 0; scanf("%d %d %d",&n,&m,&k);
    for(int i = 1; i <= n; i++){scanf("%s",map[i]+1);}
    for(int i = 1; i <= n; i++){
        for(int j = 1; j <= m; j++){
            if(map[i][j] == '.'){
                row[i][j] = 1; col[i][j] = 1;
                row[i][j] = row[i][j] + row[i-1][j];
                col[i][j] = col[i][j] + col[i][j-1];
                if(row[i][j] >= k) ans++;
                if(col[i][j] >= k) ans++;
            }
        }
    }
    if(k == 1) printf("%d\n",ans / 2);
    else printf("%d\n",ans);
    return 0;
}
// D.cpp
// o ( n + m )
#include <iostream>
#include <vector>
#include <queue>
using namespace std;
const int maxn = 300001;
int lev[maxn]; char str[maxn];
int dp[maxn][26];
vector<int> con[maxn];
int rbig(int x,int y){ return x<y?y:x; }
int main(){
    ios::sync_with_stdio(false);
    int n,m; cin >> n >> m;
    cin >> str;
    for(int i = 1; i <= m; i++){
        int a,b; cin >> a >> b;
        con[a].push_back(b);
        lev[b]++;
    }
    queue<int> q;
    for(int i = 1; i <= n; i++){
        if(lev[i] == 0) q.push(i);
    }
    int chkV = 0;
    while(!q.empty()){
        int p = q.front(); q.pop();
        dp[p][str[p-1] - 'a']++;
        chkV++;
        for(int i = 0; i < con[p].size(); i++){
            for(int j = 0; j < 26; j++) dp[con[p][i]][j] = rbig(dp[con[p][i]][j], dp[p][j]);
            if(--lev[con[p][i]] == 0) q.push(con[p][i]);
        }
    }
    if(chkV == n){
        int ans = 0;
        for(int i = 1; i <= n; i++){
            for(int j = 0; j < 26; j++){
                ans = rbig(ans,dp[i][j]);
            }}
        cout << ans << endl;
    }
    else cout << -1 << endl;
    return 0;
}