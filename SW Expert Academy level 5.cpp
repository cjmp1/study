// 3462
#include <iostream>
#include <vector>
#include <math.h>
#include <stdio.h>
using namespace std;
vector<int> prime;
int ncp[33];
int main(){
    ios::sync_with_stdio(false);
    // find_prime
    for(int i = 2; i <= 29; i++){
        bool flag = true;
        for(int j = 2; j <= (int)sqrt(i); j++){
            if(i%j == 0){ flag = false; break; }
        }
        if(flag) prime.push_back(i);
    }
    ncp[2] = 435; ncp[3] = 4060; ncp[5] = 142506; ncp[7] = 2035800; ncp[11] = 54627300; 
    ncp[13] = 119759850; ncp[17] = 119759850; ncp[19] = 54627300; ncp[23] = 2035800; ncp[29] = 30;
    int t; cin >> t; while(t--){
        int a,b,rp,c1,c2; cin >> a >> b;
        long double x,y;
        double ans = 0;
        for(int i = 0; i < prime.size(); i++){
            int p = prime[i];
            x = (long double)ncp[p];
            rp = p; while(rp--) x = x * (a / 100.0);
            rp = 30 - p; while(rp--) x = x * ((100.0-a) / 100.0);
            ans = ans + x;
            x = (long double)ncp[p];
            rp = p; while(rp--) x = x * (b / 100.0);
            rp = 30 - p; while(rp--) x = x * ((100.0-b) / 100.0);
            ans = ans + x;
        }
        for(int  i = 0; i < prime.size(); i++){
            for(int j = 0; j < prime.size(); j++){
                int p = prime[i], q = prime[j];
                x  = (long double)ncp[p]; y = (long double)ncp[q];
                rp = p; while(rp--) x = x * (a / 100.0);
                rp = 30 - p; while(rp--) x = x * ((100.0 - a) / 100.0);
                rp = q; while(rp--) y = y * (b / 100.0);
                rp = 30 - q; while(rp--) y = y * ((100.0 - b) / 100.0);
                x = x * y;
                ans = ans - x;
            }
        }
        printf("%.5f\n",ans);
    }
    return 0;
}
// 3421
#include <iostream>
#include <set>
#include <vector>
using namespace std; 
int ans,n,m; vector<int> chk[22];
set<int> S;
bool canI(int x){
    bool flag = true;
    for(int i = 0; i < chk[x].size(); i++){
        if(S.find(chk[x][i]) != S.end()){flag = false; break;}
    }
    return flag;
}
void dfs(int bef){
    ans++;
    for(int i = bef+1; i <= n; i++){
        if(canI(i)){ S.insert(i); dfs(i); S.erase(i); }
    }
}
int main(){
    ios::sync_with_stdio(false);
    int T; cin >> T;
    for(int test = 1; test <= T; test++){
        ans = 0;
        cin >> n >> m;
        for(int i = 1; i <= m; i++){
            int a,b; cin >> a >> b;
            chk[a].push_back(b);
            chk[b].push_back(a);
        }
        dfs(0);
        cout << "#" << test << " " << ans << endl;
        for(int i = 1; i <= n; i++) chk[i].clear();
    }
    return 0;
}
// 3433
#include <iostream>
#include <vector>
using namespace std;
vector<pair<char,long long> > A,B,C,D,E;
int main(){
    ios::sync_with_stdio(false);
    int T; cin >> T;
    for(int test = 1; test <= T; test++){
        char w; long long r; 
        while(A.size()) A.pop_back();
        while(B.size()) B.pop_back();
        while(C.size()) C.pop_back();
        while(D.size()) D.pop_back();
        while(E.size()) E.pop_back();
        while(true){ cin >> w; if(w == '$'){ break; }
        cin >> r; A.push_back(make_pair(w,r)); E.push_back(make_pair(w,r));}
        while(true){ cin >> w; if(w == '$'){ break; }
        cin >> r; B.push_back(make_pair(w,r)); }
        while(true){ cin >> w; if(w == '$'){ break; }
        cin >> r; C.push_back(make_pair(w,r)); }
        cout << "#" << test << " ";
        for(int a = 0; a < A.size(); a++){
            if(A[a].first == B[0].first && A[a].second >= B[0].second){ int a1 = a+1;
                bool flag = true;
                for(int b = 1; b < B.size(); b++){
                    if(a1 >= A.size()){ flag = false; break;}
                    if(!(A[a1] == B[b] || (b == (B.size() - 1) && B[b].first == A[a1].first && B[b].second <= A[a1].second))){
                        flag = false; break;
                    }
                    a1++;
                }a1--;
                if(flag){
                    while(E.size()) E.pop_back();
                    for(int j = 0; j < a; j++){ D.push_back(A[j]); }
                    if(B.size() != 1) D.push_back(make_pair(A[a].first,A[a].second-B[0].second));
                    for(int j = 0; j < C.size(); j++){ D.push_back(C[j]); }
                    if(B.size() == 1) D.push_back(make_pair(A[a].first,A[a].second-B[0].second));
                    if(B.size() != 1) D.push_back(make_pair(A[a1].first,A[a1].second-B[B.size()-1].second));
                    for(int j = a1+1; j < A.size(); j++){ D.push_back(A[j]); }
                    E.push_back(D[0]);
                    //cout << D[0].first << " " << D[0].second << " ";
                    for(int j = 1; j < D.size(); j++){
                        //cout << D[j].first << " " << D[j].second << " ";
                        if(E.back().first == D[j].first){E.back().second += D[j].second;}
                        else if(D[j].second != 0) E.push_back(D[j]);
                    }
                    
                    break;
                }
            }
        }
       // cout << endl;
        for(int i = 0; i < E.size(); i++){
            if(E[i].second != 0) cout << E[i].first << " " << E[i].second << " ";
        }
        cout << "$" << endl;
    }
    return 0;
}
// 3503
#include <iostream>
#include <algorithm>
#include <deque>
using namespace std;
deque<int> Q;
int z(int x){
    if(x < 0) return -x;
    return x;
}
int m(int x,int y){
    if(x < y) return y;
    return x;
}
int h[10111];
int main(){
    ios::sync_with_stdio(false);
    int t; cin >> t;
    for(int test = 1; test <= t; test++){
        cout << "#" << test << " ";
        int n,ans = -1; cin >> n;
        for(int i = 1; i <= n; i++){cin >> h[i];}
        sort(h+1,h+n+1); Q.clear();
        h[0] = h[1];
        Q.push_back(h[n]); Q.push_back(h[n-1]);
        for(int i = n-2; i >= 1; i-=2){
            int a = Q.front(), b = Q.back();
            int t1 = m(z(a - h[i]),z(b - h[i-1])), t2 = m(z(a - h[i-1]),z(b - h[i]));
            if(t1 < t2){ ans = ans < t1 ? t1 : ans;
                Q.push_back(h[i-1]); Q.push_front(h[i]);}
            else{  ans = ans < t2 ? t2 : ans;
                Q.push_back(h[i]); Q.push_front(h[i-1]); }
                
            if(i == 1){ cout << ans << endl; }
            else if(i == 2){
                int p = z(Q.front() - Q.back());
                ans = ans < p ? p : ans;
                cout << ans << endl;
                break;
            }
        }
    }
    return 0;
}
// 3410
#include <iostream>
using namespace std;
char we[101][10111];
int w[10111],e[10111];
int main(){
    ios::sync_with_stdio(false);
    int t; cin >> t;
    for(int test = 1; test <= t; test ++){
        cout << "#" << test << " ";
        int n, m, ans = -1, mw, c; cin >> n >> m;
        mw = 0x7fffffff;
        for(int i = 1; i <= m; i++){ cin >> we[i] + 1; }
        for(int j = 0; j <= n; j++){
            c = 0;
            for(int i = 1; i <= m; i++){
                if(we[i][j] == 'E') c++;
            }
            e[j] = j < 1 ? c : (e[j-1] + c);
           // cout << e[j] << " ";
        }
        //cout << endl;
        for(int j = n; j >= 0; j--){
            c = 0;
            for(int i = 1; i <= m; i++){
                if(we[i][j+1] == 'W') c++;
            }
            w[j] = w[j+1] + c;
            //cout << w[j] << " ";
        }
        //cout << endl;
        for(int i = 0; i <= n; i++){
            if(mw > (w[i]+e[i])){ mw = w[i] + e[i]; ans = i; }
        }
        cout << ans << " " << ans+1 << endl;
    }
    return 0;
}
// 3308
#include <stdio.h>
#include <algorithm>
using namespace std;
const int maxn = 100111;
struct lisform{
    int basesu;
    int renum;
    int index;
}longest[maxn];
int indextree[maxn*3];
int tab[maxn],pathlis[maxn],dap[maxn];
int cmp1(lisform x,lisform y){
    if(x.basesu < y.basesu) return 1;
    return 0;
}
int cmp2(lisform x,lisform y){
    if(x.index < y.index) return 1;
    return 0;
}
int main(){
    
    int x, t, cnt, mx1, dmax, s, e; scanf("%d",&t); 
    for(int test = 1; test <= t; test++){ printf("#%d ",test);
        dmax = -1;
        int n; scanf("%d",&n);
        for(int i = 1; i <= n; i++){
            tab[i] = 0;
            scanf("%d",&longest[i].basesu); 
            longest[i].index = i;
        }
        sort(longest+1,longest+n+1,cmp1);
        x = cnt = longest[1].renum = 1;
        for(int i = 2; i <= n; i++){
            if(longest[i].basesu != longest[i-1].basesu){
                longest[i].renum = ++cnt;}
            else longest[i].renum = cnt;
        }
        std::sort(longest+1,longest+n+1,cmp2);
        while(x < cnt){ x <<= 1; }
        for(int i = 0; i <= x * 2 - 1; i++) indextree[i] = 0;
        for(int i = 1; i <= n; i++){
            s = x; e = x + longest[i].renum - 1; 
            mx1 = 0;
            while(s < e){
                if(s&1){
                    if(mx1 < indextree[s]){ mx1 = indextree[s]; };
                    s++;
                }
                if(!(e&1)){
                    if(mx1 < indextree[e]){ mx1 = indextree[e]; };
                    e--;
                }
                s >>= 1;
                e >>= 1;
            }
            if(s == e){ if(mx1 < indextree[e]){ mx1 = indextree[e]; }; }
            tab[i] = mx1 + 1;
            int x1 = longest[i].renum + x - 1;
            while(tab[i] > indextree[x1] && x1 > 0){
                indextree[x1] = tab[i];
                x1 >>= 1;
            }
            dmax = dmax < tab[i] ? tab[i] : dmax;
        }
        printf("%d\n",dmax);
    }
    return 0;
}
//3269
#include <iostream>
#include <algorithm>
#include <memory.h>
#include <vector>
#include <set>
using namespace std;
int N, M,cnt = 0,cn = 0;
vector<int> con[10111],rev[10111];
bool chk[10111];
int tab[10111],pl[10111];
set<int> ss[10111];
void dfs(int p){
	for (int i = 0; i < con[p].size(); i++){
		if (!chk[con[p][i]]){
			chk[con[p][i]] = true;
			dfs(con[p][i]);
		}
	}
	tab[++cnt] = p;
}
void bdfs(int p){
	for (int i = 0; i < rev[p].size();  i++){
		if (!chk[rev[p][i]]){
			ss[cn].insert(rev[p][i]);
			chk[rev[p][i]] = true;
			bdfs(rev[p][i]);
		}
	}
}
int main(){
    ios::sync_with_stdio(false);
    int t; cin >> t;
    for(int test = 1; test <= t; test++){ cout << "#" << test << " ";
        cnt = cn = 0;
	    cin >> N >> M;
	    for (int i = 1; i <= M; i++){
		    int a, b;
		    cin >> a >> b;
		    con[a].push_back(b);
    		rev[b].push_back(a);
	    }
	    for (int i = 1; i <= N; i++){
		    pl[i] = -1;
		    if (!chk[i]){
			    chk[i] = true;
			    dfs(i);
    		}
	    }
	    memset(chk, false, sizeof(chk));
	    for (int i = N; i >= 1; i--){
		    if (!chk[tab[i]]){
			    chk[tab[i]] = true;
			    ss[++cn].insert(tab[i]);
			    bdfs(tab[i]);
			    pl[*ss[cn].begin()] = cn;
    		}
	    }
	    cout << cn << "\n";
	    for(int i = 1; i <= N; i++){
	        con[i].clear();
	        rev[i].clear();
	        chk[i] = false;
	    }
	    for(int i = 1; i <= cn; i++) ss[i].clear();
    }
	
	//for (int i = 1; i <= N; i++){
		//if (pl[i] != -1){
			//set<int>::iterator pos;
			//for (pos = ss[pl[i]].begin(); pos != ss[pl[i]].end(); ++pos){
				//cout << *pos << " ";
			//}
			//cout << -1 << "\n";
		//}
	//}
	
	return 0;
}
//3339
#include <iostream>
#include <math.h>
#include <map>
using namespace std;
const unsigned long long MOD = 1000000007;
map<unsigned long long,int> p;
unsigned long long a[111111];
void dfs(unsigned long long n){
    bool flag = true;
    for(unsigned long long i = 2; i <= (unsigned long long)sqrt(n); i++){
        if(n%i == 0){
            flag = false;
            dfs(i);
            dfs(n/i);
            break;
        }
    }
    if(flag){
        map<unsigned long long,int>::iterator k;
        if(p.find(n) != p.end()){
            k = p.find(n);
            k->second++;
        }
        else p.insert(make_pair(n,1));
    }
}
unsigned long long int getp(unsigned long long int r, unsigned long long int n){
    if (n == 0){ return 1; }
    unsigned long long memo[64] = { 0 };
    memo[0] = r % 1000000007;
    for (int i = 1; i < 64 && ((unsigned long long)1 << i - 1) < n; i++){
        memo[i] = (memo[i - 1] * memo[i - 1]) % 1000000007;
    }
    unsigned long long result = 1;
    for (int i = 0; i < 64; i++){
        if ((n &((unsigned long long)1 << i)) != 0){
            result = (result*memo[i]) % 1000000007;
        }
    }
    return result;
}
int main(){
    ios::sync_with_stdio(false);
    int t; cin >> t;
    for(int test = 1; test <= t; test++){
        p.clear();
        cout << "#" << test << " ";
        unsigned long long n,m; cin >> n >> m;
        if(n == 1) cout << 1 << endl;
        else{
        dfs(n);
        map<unsigned long long,int>::iterator i;
        int c = 1;
        for(i = p.begin(); i != p.end(); ++i){
            unsigned long long A = (getp(i->first,i->second*m+1) + MOD - 1)%MOD;
            unsigned long long B = getp(i->first-1,MOD-2);
            a[c] = (A * B) % MOD;
            c++;
        }
        unsigned long long ans = a[1];
        for(int j = 2; j < c; j++){
            ans  = (ans * a[j]) % MOD;
        }
        cout << ans << endl;
        }
    }
    return 0;
}