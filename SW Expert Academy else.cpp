//2477
#include <iostream>
#include <deque>
#include <algorithm>
using namespace std;
const int maxk = 1011; 
const int maxn = 10, maxm = 10;
struct Cnumber{
    int ti,num;
}P[maxk],chkA[maxn],chkB[maxm],ct;
int A[maxn],B[maxm];
deque<int> Await,Bwait;
bool ua[maxk];
bool cp(Cnumber x,Cnumber y){
    if(x.ti > y.ti) return 0;
    if(x.ti == y.ti && x.num > y.num) return 0;
    return 1;
}
int main(){
    ios::sync_with_stdio(false);
    int t; cin >> t;
    for(int T = 1; T <= t; T++){ cout << "#" << T << " ";
        int n,m,k,a,b, ans = 0; cin >> n >> m >> k >> a >> b;
        ct.ti = 0; ct.num = -1;
        for(int i = 1; i <= n; i++){cin >> A[i]; chkA[i] = ct; chkB[i] = ct; }
        for(int i = 1; i <= m; i++){cin >> B[i];}
        for(int i = 1; i <= k; i++){
            ua[i] = false;
            int x; cin >> x; ct.ti = x; ct.num = i;
            P[i] = ct;
        }
        sort(P+1,P+k+1,cp);
        int nt = 0 , np = 1, cb = 0;
        while(cb < k){
            while(np <= k){
                if(P[np].ti == nt){ Await.push_back(P[np++].num); }
                else break;
            }
            
            for(int i = 1; i <= n; i++){
                if(chkA[i].ti > 0) chkA[i].ti--;
                if(chkA[i].ti == 0 && chkA[i].num != -1){ Bwait.push_back(chkA[i].num); chkA[i].num = -1; }
                if(chkA[i].num == -1 && Await.size() != 0){
                    ct.ti = A[i]; ct.num = Await.front();
                    chkA[i] = ct;
                    if(i == a) ua[Await.front()] = true;
                    Await.pop_front();
                }
            }
            
            for(int i = 1; i <= m; i++){
                if(chkB[i].ti > 0) chkB[i].ti--;
                if(chkB[i].ti == 0 && chkB[i].num != -1){ chkB[i].num = -1; cb++; }
                if(chkB[i].num == -1 && Bwait.size() != 0){
                    ct.ti = B[i]; ct.num = Bwait.front();
                    chkB[i] = ct;
                    if(i == b && ua[Bwait.front()]) ans+= Bwait.front();
                    Bwait.pop_front();
                }
            }
            //cout << nt << endl;
            //for(int i = 0; i < Await.size(); i++) cout << Await[i] << " ";
            //cout << endl;
            //for(int i= 1; i <= n; i++) cout << chkA[i].num << " ";
            //cout << endl;
            //for(int i = 0; i < Bwait.size(); i++) cout << Bwait[i] << " ";
            //cout << endl;
            //for(int i= 1; i <= m; i++) cout << chkB[i].num << " ";
            //cout << endl;
            //cout << "----------------------" << endl;
            nt++;
        }
        if(ans == 0) cout << -1 << endl;
        else cout << ans << endl;
    }
    return 0;
}