// week 2 
% warmUpExercise.m 
A = eye(5);
% plotData.m
plot(x,y, 'rx', 'MarkerSize', 10);
ylabel('Profit in $10,000s');
xlabel('Population of City in 10,000s');
hold on;
axis([4 24 -5 25]);
% computeCost
J = sum((X * theta - y).^2) / (2*m);
% gradientDescent
theta = theta - (alpha * (1/m) * ((X * theta - y)' * X))';
% featureNormalize
for i = 1 : size(X,2)
  mu(i) = mean(X(:,i));
  sigma(i) = std(X(:,i));
end

for i = 1 : size(X,2)
  for j = 1 : size(X,1)
    X_norm(j,i) = (X(j,i) - mu(i)) / sigma(i);
  end
end
% computeCostMulti
J = 1 / (2 * m) * (X * theta - y)' * (X * theta - y); // vectorized
% gradientDescentMulti
theta = theta - (alpha * (1/m) * sum((X * theta - y).*X))'; 
theta = theta - (alpha * (1/m) * X' * (X * theta - y)); // vectorized
% normalEqn
theta = pinv(X' * X) * X' * y;
// week 3
% costFunction.m
h = sigmoid(X * theta);
J = (1 / m) * sum ( -y .* log(h) - (1 - y) .* log(1 - h) );
grad = (1 / m) * ((h - y)' * X)
% costFunctionReg.m
h = sigmoid(X * theta);
A = theta(2:size(theta),1);
J = (1 / m) * sum ( -y .* log(h) - (1 - y) .* log(1 - h) ) + (lambda / (2 * m)) * sum(A.^2);
grad(1) = sum((sigmoid(X*theta).-y).*X(:,1))/m;
for i = 2:size(theta,1)
	grad(i) = (sum((sigmoid(X*theta).-y).*X(:,i))+lambda*theta(i))/m;
end
% plotData.m
pos = find(y == 1); neg = find(y == 0);
plot(X(pos,1), X(pos, 2), 'k+', 'LineWidth', 2, 'MarkerSize',7);
plot(X(neg,1), X(neg, 2), 'ko', 'MarkerFaceColor', 'y', 'MarkerSize',7);
% sigmoid
g = 1.0 / (1 + exp(-z));
% predict
p = sigmoid(X * theta) >= 0.5;
// week 4
% lrcostfunction.m
h = sigmoid(X * theta);
temp = [0; theta(2: end)]; 
J = (1 / m) * (-y' * log(h) - (1-y') * log(1-h)) + (lambda / (2 * m)) * (temp' * temp);
grad = (1 / m) * ( X' * (h-y)) + ((lambda / m) * temp);
% onevsall.m
for k = 1:num_labels
    initial_theta = zeros(n + 1, 1);
    options = optimset('GradObj', 'on', 'MaxIter', 50);
    [theta] = fmincg(@(t)(lrCostFunction(t, X, (y==k), lambda)), initial_theta, options);
	  all_theta(k, :) = theta' ;
end  
% predictonevsall.m
[c, i] = max(sigmoid(X * all_theta'), [], 2);
p = i;
% predict.m
A1 = [ones(1, m); X' ];
X2 = sigmoid(Theta1 * A1);
A2 = [ones(1, m); X2 ];
A3 = sigmoid(Theta2 * A2);
[x, xi] = max(A3' , [], 2);
p = xi;
// week 5
% sigmoidgradient.m
for i = 1:size(z,1)
    for j = 1:size(z,2)
        g(i,j) = sigmoid(z(i,j)) * (1 - sigmoid(z(i,j)));
    end
end
% randinitializeWeights.m
epsilon_init = 0.12;
W = rand(L_out, 1 + L_in) * 2 * epsilon_init - epsilon_init;
% nncostfunction.m
%% follow the neural network to compute the hypothesis
a_1 = [ones(m, 1) X];
z_2 = a_1 * Theta1';
a_2 = [ones(m,1) sigmoid(z_2)];
z_3 = a_2 * Theta2';
a_3 = sigmoid(z_3);
hypothesis = a_3;

%recode the labels as vectors
vector_y = diag(ones(1,num_labels),0);

%% compute the Regularized cost function J
%1.compute the J without regularization
for i = 1:m
    J =J + (-log(hypothesis(i,:)) * vector_y(:,y(i)) - log(1-hypothesis(i,:)) * (1-vector_y(:,y(i))));
end
J = J/m;
%2.add regularization to your cost function
Theta1_Total = 0;
for i = 1:size(Theta1,1)
    for j = 2:size(Theta1,2)
        Theta1_Total = Theta1_Total + Theta1(i,j) ^2;
    end
end
Theta2_Total = 0;
for i = 1:size(Theta2,1)
    for j = 2:size(Theta2,2)
        Theta2_Total = Theta2_Total + Theta2(i,j) ^2;
    end
end
J = J + lambda * (Theta1_Total + Theta2_Total) / (2 * m);


Diff_1 = zeros(size(Theta1,1),size(Theta1,2));
Diff_2 = zeros(size(Theta2,1),size(Theta2,2));
%% backpropagation algorithm 
%1.compute the gradient without regularization
for i = 1:m
    a_1 = [1 X(i,:)];
    z_2 = a_1 * Theta1';
    a_2 = [1 sigmoid(z_2)];
    z_3 = a_2 * Theta2';
    a_3 = sigmoid(z_3);
    
    delta_3 = a_3' - vector_y(:,y(i));
    delta_2 = Theta2' * delta_3 .* sigmoidGradient([1 z_2]') ;
    % Note that you should skip or remove the first element of delta_2
    delta_2 = delta_2(2:end);
    
    Diff_2 = Diff_2 + delta_3 * a_2;
    Diff_1 = Diff_1 + delta_2 * a_1;
    
end
Theta2_grad = Diff_2 / m;
Theta1_grad = Diff_1 / m;

%2.add regularization to gradient
for i = 1:size(Theta1,1)
    for j = 2:size(Theta1,2)
        Theta1_grad(i,j) = Theta1_grad(i,j) + lambda * Theta1(i,j) / m;
    end
end
for i = 1:size(Theta2,1)
    for j = 2:size(Theta2,2)
        Theta2_grad(i,j) = Theta2_grad(i,j) + lambda * Theta2(i,j) / m;
    end
end
// week 6
% linearRegCostFuntion.m
%output of hypothesis
hypothesis = X * theta;
%compute the J
J = sum((hypothesis - y) .^ 2) / (2 * m) + (sum(theta(2:size(theta)) .^ 2)) * lambda / (2 * m);

%compute the gradient
%formula of grad_1 is different from the other gradient.
grad(1)= X(:,1)' * (hypothesis - y) / m; 
grad(2:size(grad)) = X(:,2:size(grad))' * (hypothesis - y) / m + theta(2:size(theta)) * lambda / m;

%learningCurve.m

for i = 1:m
    % Compute train/cross validation errors using training examples X(1:i, :) and y(1:i)
    [theta] = trainLinearReg([ones(i,1) X(1:i, :)],y(1:i),lambda);
    J_train = linearRegCostFunction([ones(i, 1) X(1:i, :)], y(1:i), theta, 0);
    J_val = linearRegCostFunction([ones(size(Xval, 1), 1) Xval], yval, theta, 0);
    
    % storing the result in error_train(i) and error_val(i)
    error_train(i) = J_train;
    error_val(i) = J_val;

end

% polyFeature.m
for i = 1:p
    X_poly(:, i) = X.^i;
end

% validationCurve.m
for i = 1:1:length(lambda_vec)
    lambda = lambda_vec(i);
    % Compute train/cross validation errors using training examples X(1:i, :) and y(1:i)
    [theta] = trainLinearReg([ones(size(X, 1),1) X],y,lambda);
    J_train = linearRegCostFunction([ones(size(X, 1), 1) X], y, theta, 0);
    J_val = linearRegCostFunction([ones(size(Xval, 1), 1) Xval], yval, theta, 0);
    
    % storing the result in error_train(i) and error_val(i)
    error_train(i) = J_train;
    error_val(i) = J_val;

end

// week 7

% findClosestCentroids.m

for i = 1:size(X,1)
  for j = 1:K
    distance(j) = sum((centroids(j,:) - X(i,:)).^2);
  end
  [closest_distance,I] = min(distance);
  idx(i) = I;
end

% computeCentroids.m

K_number = zeros(K,1);

for i = 1:m
  centroids(idx(i),:) = centroids(idx(i),:) + X(i,:); % centroids에 포함된 점의 xy 합
  K_number(idx(i)) = K_number(idx(i)) + 1; % 포함된 점의 수
endfor

for i = 1:K
  centroids(i,:) = centroids(i,:) ./ K_number(i);
endfor


% KmeansinitCentroids.m

randidx = randperm(size(X, 1));
centroids = X(randidx(1:K), :);

% pca.m

Sigma = (X' * X) / m;
[U, S, V] = svd(Sigma);

% projectdata.m

for i=1:size(X,1)
    x = X(i, :)';
    Z(i,:) = x' * U(:, 1:K);
end

% recoverdata.m

for i = 1:size(Z,1)
  v = Z(i,:)';
  X_rec(i,:) = v' * U(:,1:K)';
end

// week 8

% estimateGaussian.m

mu = sum(X) ./ m;
sigma2 = (sum(X .^ 2) - 2 * mu .* ( sum(X)) + m * mu .^ 2) / m;

% selectThreshold.m

cvPredictions = (pval < epsilon);
tp = sum((cvPredictions == 1) & (yval == 1));
fp = sum((cvPredictions == 1) & (yval == 0));   fn = sum((cvPredictions == 0) & (yval == 1));
prec = tp / (tp + fp);
rec = tp / (tp + fn);
F1 = 2 * prec * rec / (prec + rec);

% cofiCostFunction.m

J = sum(sum((((X * Theta' - Y ) .^ 2) .* R))) / 2 + ...
    sum(sum(Theta .^ 2)) * lambda / 2 + sum(sum(X .^ 2)) * lambda / 2;

X_grad = ((X * Theta' - Y ) .* R) * Theta + lambda * X;

Theta_grad = ((X * Theta' - Y ) .* R)' * X + lambda * Theta;